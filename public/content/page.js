let content = `
# The amazing project

Date: put the date of the workshop here

## Details

You will learn:
- a lot of awesome things

## Pre-requisites

- Git
- GitLab
- ...

## How to register for the workshop?

- Go to the **[registrations]()** project
- Create an issue (define rules if needed)
- The day before, you'll be able to access to the **[communications]()** project with all the details (Zoom link)
`

let page =  {
  content: content
}

export {page}
