import {Style, Template, Ossicle} from '../node_modules/@ossicle/ossicle.js/Ossicle.js'

import {config} from '../content/config.js'

const mainTitle = config.title
const subTitle = config.subTitle

class MainTitle extends Ossicle {

  static sheets() { return [
    Style.sheets.bulma
  ]}

  static template() { return Template.html`
    <section class="hero is-bold"> <!--  is-medium is-light -->
      <div class="hero-body">
        <div class="container">
          
          <h1 class="title is-1">
            ${mainTitle}
          </h1>
          
          <h2 class="subtitle">
            ${subTitle}
          </h2>

        </div>
      </div>
    </section>
  `
  }

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-title', MainTitle)

